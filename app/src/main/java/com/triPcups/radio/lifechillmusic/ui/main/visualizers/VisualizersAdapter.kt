package com.triPcups.radio.lifechillmusic.ui.main.visualizers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.hardik.clickshrinkeffect.applyClickShrink
import com.triPcups.radio.lifechillmusic.R
import com.triPcups.radio.lifechillmusic.ui.app_data.AppDatabase
import com.triPcups.radio.lifechillmusic.ui.models.Visualizer

class VisualizersAdapter(private val listener: VisualizersAdapterListener) : RecyclerView.Adapter<VisualizersAdapter.VisualizerViewHolder>() {

    interface VisualizersAdapterListener {
        fun onVisualizerSelected(visualizer: Visualizer)
    }

    private val visualizers = AppDatabase.getVisualizers()
    public var currentlySelectedIndex = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VisualizerViewHolder {
        return VisualizerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.visualizer_item, parent, false))
    }

    override fun onBindViewHolder(holder: VisualizerViewHolder, position: Int) {
        val visualizer = visualizers[position]
        val isSelected = currentlySelectedIndex == visualizer.index
        holder.bind(visualizer, isSelected)
    }

    override fun getItemCount(): Int {
        return visualizers.size
    }

    private fun onVisualizerClick(visualizer: Visualizer) {
        listener.onVisualizerSelected(visualizer)

        currentlySelectedIndex = visualizer.index
        notifyDataSetChanged()
    }

    inner class VisualizerViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        private val visualizerTitle: TextView = v.findViewById(R.id.visualizerName)
        private val visualizerImage: ImageView = v.findViewById(R.id.visualizerImage)
        private val cardView : CardView = v.findViewById(R.id.visualizerImageLayout)

        fun bind(visualizer: Visualizer, isSelected: Boolean) {
            cardView.applyClickShrink()
            visualizerTitle.text = visualizer.name
            visualizerImage.setImageResource(visualizer.imgResId)

            visualizerTitle.isActivated = isSelected
            cardView.isActivated = isSelected

            itemView.setOnClickListener {
                onVisualizerClick(visualizer)
            }
            cardView.setOnClickListener {
                onVisualizerClick(visualizer)
            }
        }
    }
}