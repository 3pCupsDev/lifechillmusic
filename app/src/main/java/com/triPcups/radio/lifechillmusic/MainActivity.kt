package com.triPcups.radio.lifechillmusic

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.*
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.MotionEvent
import android.view.WindowManager
import android.widget.Button
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.triPcups.radio.lifechillmusic.ui.main.MainFragment
import java.util.*


class MainActivity : AppCompatActivity() , MainFragment.MainFragmentListener{


    companion object {

        val CHANNEL_ID: String = "LiFE CHiLL MUSiC_silent"
        val MICROPHONE_REQUEST_CODE: Int = 420

    }


    private var lastColor: Int = 0
    private var frameContainer: FrameLayout? = null
    private var mAdView: AdView? = null
    private var isExiting: Boolean = false
    private var notificationManager: NotificationManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        frameContainer = findViewById<FrameLayout>(R.id.container)
        lastColor = ContextCompat.getColor(this, R.color.colorPrimary)

        createNotificationChannel()
        initDismissNotificiationService()
        initColorizedStatusBar()
        initAds()



        if (savedInstanceState == null) {
            showFragment(MainFragment.newInstance(), MainFragment::class.java.simpleName)
        }
    }

    private fun initColorizedStatusBar() {
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    }

    private fun initAds() {
        //for general use (banner && app open)
        MobileAds.initialize(this) {}
        //for banner
        val bannerAdUnitId = if(BuildConfig.DEBUG) {
            "ca-app-pub-3940256099942544/6300978111"
        } else {
            "ca-app-pub-7481638286003806/6610437312"
        }
        val adViewContainer = findViewById<FrameLayout>(R.id.adViewContainer)
        mAdView = AdView(this)
        val adRequest = AdRequest.Builder().build()
        mAdView?.apply {
            adUnitId = bannerAdUnitId
            adSize = AdSize.BANNER
            loadAd(adRequest)
        }

        adViewContainer.addView(mAdView)
    }

    private fun initDismissNotificiationService() {
        val filter = IntentFilter("android.intent.CLOSE_ACTIVITY")
        registerReceiver(mReceiver, filter)
    }

//    override fun onResumeFragments() {
//        super.onResumeFragments()
//
//        showNotification()
//    }

    override fun onPause() {
        super.onPause()

        if(!isExiting) {
            showNotification()
        }
    }

    override fun onResume() {
        super.onResume()

        isExiting = false
        notificationManager?.cancelAll()
    }

    private fun showFragment(fragment: Fragment, tag: String) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment, tag)
                .commitNow()
    }

    private fun getFragmentByTag(tag: String) : Fragment? {
        return supportFragmentManager.findFragmentByTag(tag)
    }

//    override fun onConfigurationChanged(newConfig: Configuration) {
//        super.onConfigurationChanged(newConfig)
//
//        when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
//            Configuration.UI_MODE_NIGHT_YES ->
//                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
//            Configuration.UI_MODE_NIGHT_NO ->
//                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
//        }
//    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == MICROPHONE_REQUEST_CODE) {
            val mainFrag = getFragmentByTag(MainFragment::class.java.simpleName) as MainFragment?
            if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mainFrag?.apply {
                    initAllVisualizers()
                    playNextVisualizer()
                    setVisualizersBtnVisibility(true)
                }
                Toast.makeText(this, "Now you can enjoy Visualizers", Toast.LENGTH_LONG).show()
            } else {
                mainFrag?.setVisualizersBtnVisibility(false)
                Toast.makeText(
                    this,
                    "This app needs microphone permission so you could enjoy Visualizers",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.app_name)
            val descriptionText = "A simple system notification to show and control played radio music"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
                setSound(null, null)
            }

            // Register the channel with the system
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager?.createNotificationChannel(channel)
        }
    }

    @SuppressLint("WrongConstant", "UnspecifiedImmutableFlag")
    private fun showNotification() {
        val deleteIntent = Intent("android.intent.CLOSE_ACTIVITY")
        val deletePendingIntent = PendingIntent.getBroadcast(this, 0, deleteIntent, 0)

        val mBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_note_white) // notification icon
            .setContentTitle("Listening to " + getString(R.string.app_name))//getString(R.string.app_name)) // title for notification
            .setContentText("Swipe to close")//"Listening to LIFE CHILL MUSiC") // message for notification
//            .setSubText("Swipe to close")
            .setAutoCancel(true) // clear notification after click
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setChannelId(CHANNEL_ID)
            .setDefaults(Notification.DEFAULT_ALL)
//            .setSound()
            .setLocalOnly(true) //todo check what this does
            .setContentIntent(
                PendingIntent.getActivity(
                    this, 0, Intent(
                        this,
                        MainActivity::class.java
                    ), Intent.FLAG_ACTIVITY_NEW_TASK
                )
            )
            .setDeleteIntent(deletePendingIntent)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)  //to show content in lock screen

//        val intent = Intent(this, MainActivity::class.java)
//        val pi = PendingIntent.getActivity(this, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK)
//        mBuilder.setContentIntent(pi)
//        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager?.notify(0, mBuilder.build())
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
       return when (event.action) {
//            MotionEvent.ACTION_UP,
           MotionEvent.ACTION_DOWN -> {
               val mainFrag = getFragmentByTag(MainFragment::class.java.simpleName) as MainFragment?
               mainFrag?.toggleVisualizersPanel()
               false
           }
                else -> super.onTouchEvent(event)
//            else -> {
//            }
        }
    }

   override fun onBackPressed() {
        val mainFrag = getFragmentByTag(MainFragment::class.java.simpleName) as MainFragment?

        if (mainFrag != null && mainFrag.closeColorPickerDialogIfOpen()) {
            //closed Color picker dialog
        } else if (mainFrag != null && mainFrag.isVisualizersPanelOpen()) {
            mainFrag.toggleVisualizersPanel()
        } else if (mainFrag != null && mainFrag.isStylingPanelOpen()) {
            mainFrag.toggleStylingPanel()
        } else {
            val dialog = AlertDialog.Builder(this)
                .setTitle("What would you like to do now?")
                .setPositiveButton("Exit") { dialog, which ->
//                super.onBackPressed()
                    //                val mainFrag = getFragmentByTag(MainFragment::class.java.simpleName) as MainFragment?
////                mainFrag?.stopAllVisualizers()
//                mainFrag?.onDestroy()
////                onDestroy()
                    isExiting = true
                    finish()
                }
                .setNegativeButton("Listen in Background") { dialog, which ->
//                showNotification()
                    isExiting = false

                    val backgroundIntent = Intent(Intent.ACTION_MAIN)
                    backgroundIntent.addCategory(Intent.CATEGORY_HOME)
                    startActivity(backgroundIntent)

                }
                .create()
            dialog.show()

            val b: Button = dialog.getButton(DialogInterface.BUTTON_POSITIVE)
            val b2: Button = dialog.getButton(DialogInterface.BUTTON_NEGATIVE)

            if (b != null) {
                b.setTextColor(lastColor)
                b2.setTextColor(lastColor)
            }
        }
    }

    override fun onDestroy() {
        notificationManager?.cancelAll()
        super.onDestroy()
    }

    override fun finish() {
        notificationManager?.cancelAll()
        super.finish()
    }

    private var mReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if(!this@MainActivity.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
                finish()
//            } else {
            }
        }
    }

    override fun onSetColor(color: Int) {
        // finally change the color
        window.statusBarColor = color
        lastColor = color

//        showGlowBackground(Random.nextBoolean())
//        frameContainer?.backgroundTintList = if(Random.nextBoolean()) {
//            ColorStateList.valueOf(color)
//        } else {
//            ColorStateList.valueOf(resources.getColor(android.R.color.transparent))
//        }
    }

    override fun showGlowBackground(isVisible: Boolean) {
        frameContainer?.backgroundTintList = ColorStateList.valueOf(if(isVisible) {
            lastColor
        } else {
            ContextCompat.getColor(this, android.R.color.transparent)
        })
    }
}