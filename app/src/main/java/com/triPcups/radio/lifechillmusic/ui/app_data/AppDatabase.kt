package com.triPcups.radio.lifechillmusic.ui.app_data

import com.triPcups.radio.lifechillmusic.R
import com.triPcups.radio.lifechillmusic.ui.models.Visualizer

class AppDatabase {

    companion object {

        fun getVisualizers() : ArrayList<Visualizer> {
            val visualizers = arrayListOf<Visualizer>()

            visualizers.add(Visualizer(-1, "Normal", R.mipmap.ic_launcher))
            visualizers.add(Visualizer(0,"Blast", R.drawable.ic_vis_blas))
            visualizers.add(Visualizer(1, "Blob", R.drawable.ic_vis_blob))
            visualizers.add(Visualizer(2, "Bar", R.drawable.ic_vis_bar))
            visualizers.add(Visualizer(3, "Circle", R.drawable.ic_vis_circle))
            visualizers.add(Visualizer(4, "HiFi", R.drawable.ic_vis_hifi))
            visualizers.add(Visualizer(5, "Wave", R.drawable.ic_vis_wave))

            return visualizers
        }
    }
}
