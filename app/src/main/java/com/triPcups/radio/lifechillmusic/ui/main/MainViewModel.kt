package com.triPcups.radio.lifechillmusic.ui.main

import android.app.Activity
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.triPcups.radio.lifechillmusic.R


class MainViewModel : ViewModel() {
    // TODO: Implement the ViewModel


    private val RADIO_URL_TAG = "LiFE_CHiLL_MUSiC_radio_url"

    private val mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()

    val currentRadioUrl : MutableLiveData<String> = MutableLiveData()

    init {
        mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_default_map)
    }

    fun fetchRemoteConfigData(activity: Activity) {
        mFirebaseRemoteConfig.fetchAndActivate()
            .addOnCompleteListener(activity) { task ->
                val radioUrl = mFirebaseRemoteConfig.getString(RADIO_URL_TAG)
                currentRadioUrl.postValue(radioUrl)

//                if (task.isSuccessful) {
//                } else {
//
//                }
            }
    }

}