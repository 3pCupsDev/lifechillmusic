package com.triPcups.radio.lifechillmusic

import android.app.Application
import android.content.Intent
import com.google.android.gms.ads.MobileAds
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.triPcups.radio.lifechillmusic.ads.AppOpenManager
import com.triPcups.radio.lifechillmusic.common.AppExceptionHandler


class App: Application() {

    private var appOpenManager: AppOpenManager? = null


    override fun onCreate() {
        super.onCreate()

        initAdMob()
        setupCrashHandler()
    }

    private fun initAdMob() {
        MobileAds.initialize(this) { }
        appOpenManager = AppOpenManager(this)
        appOpenManager?.showAdIfAvailable()
    }

//    private fun setupCrashHandler() {
//        // 1. Get the system handler.
////        val systemHandler = Thread.getDefaultUncaughtExceptionHandler()
//
//        // 2. Set the default handler as a dummy (so that crashlytics fallbacks to this one, once set)
////        Thread.setDefaultUncaughtExceptionHandler { t, e -> /* do nothing */ }
//
//        // 3. Setup crashlytics so that it becomes the default handler (and fallbacking to our dummy handler)
//        FirebaseCrashlytics.getInstance().log("App#setupCrashHandler")
//
////        val fabricExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()
//
//        // 4. Setup our handler, which tries to restart the app.
//        Thread.setDefaultUncaughtExceptionHandler { t, e ->
//            t.run {
//                FirebaseCrashlytics.getInstance().recordException(e)
//                startActivity(Intent(applicationContext, MainActivity::class.java))
//            }
//
//        }
//    }


    private fun setupCrashHandler() {
        // 1. Get the system handler.
        val systemHandler = Thread.getDefaultUncaughtExceptionHandler()

        // 2. Set the default handler as a dummy (so that crashlytics fallbacks to this one, once set)
        Thread.setDefaultUncaughtExceptionHandler { t, e -> /* do nothing */ }

        // 3. Setup crashlytics so that it becomes the default handler (and fallbacking to our dummy handler)
        FirebaseCrashlytics.getInstance().log("App#setupCrashHandler")

        val fabricExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()

        // 4. Setup our handler, which tries to restart the app.
        Thread.setDefaultUncaughtExceptionHandler(AppExceptionHandler(systemHandler, fabricExceptionHandler, this))
    }


}