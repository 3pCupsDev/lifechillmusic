package com.triPcups.radio.lifechillmusic.ui.main

//import com.triPcups.radio.lifechillmusic.common.ColorPickerPopup

import android.Manifest
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.AudioManager.OnAudioFocusChangeListener
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gauravk.audiovisualizer.base.BaseVisualizer
import com.gauravk.audiovisualizer.model.AnimSpeed
import com.gauravk.audiovisualizer.model.PaintStyle
import com.gauravk.audiovisualizer.visualizer.*
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.triPcups.radio.lifechillmusic.MainActivity
import com.triPcups.radio.lifechillmusic.R
import com.triPcups.radio.lifechillmusic.common.OnSwipeTouchListener
import com.triPcups.radio.lifechillmusic.ui.main.visualizers.VisualizersAdapter
import com.triPcups.radio.lifechillmusic.ui.models.Visualizer
import top.defaults.colorpicker.ColorPickerPopup
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.random.Random


class MainFragment : Fragment(), VisualizersAdapter.VisualizersAdapterListener {

    interface MainFragmentListener {
        fun onSetColor(color: Int)
        fun showGlowBackground(isVisible: Boolean)
    }

    companion object {
        fun newInstance() = MainFragment()
    }

    private var glowCheckBox: CheckBox? = null
    private var listener: MainFragmentListener? = null

    private var lineTypeRadioGroupFillBtn: RadioButton? = null
    private var lineTypeRadioGroupOutlineBtn: RadioButton? = null
    private var lineTypeRadioGroup: RadioGroup? = null

    private var speedRadioGroupSlowBtn: RadioButton? = null
    private var speedRadioGroupMediumBtn: RadioButton? = null
    private var speedRadioGroupFastBtn: RadioButton? = null
    private var speedRadioGroup: RadioGroup? = null

    private var visualizationsPanel: LinearLayout? = null

    private var stylingPanel: LinearLayout? = null
    private var densityLayout: LinearLayout? = null
    private var densitySeekbar: SeekBar? = null
    private var strokeLayout: LinearLayout? = null
    private var strokeSeekbar: SeekBar? = null
    private var colorPickerDialog: ColorPickerPopup? = null
    private lateinit var visualizersAdapter: VisualizersAdapter
    private var visualizersTitleBtn: TextView? = null
    private var colorsTitleBtn: TextView? = null
    private var stylingTitleBtn: TextView? = null

    private val visualizers: ArrayList<View> = arrayListOf()
    private var mediaPlayer: MediaPlayer? = null

    private var currentVisualizer: BaseVisualizer? = null
    private lateinit var viewModel: MainViewModel

    private var oldLastColor: Int = -1
    private var lastColor: Int = -1

    private var mVisualizer: BlastVisualizer? = null
    private var mVisualizer2: BlobVisualizer? = null
    private var mVisualizer3: BarVisualizer? = null
    private var mVisualizer4: CircleLineVisualizer? = null
    private var mVisualizer5: HiFiVisualizer? = null
    private var mVisualizer6: WaveVisualizer? = null

    private var visualizersPanelRv: RecyclerView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        activity?.let { viewModel.fetchRemoteConfigData(it) }
        initObservers()
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainFragmentListener) {
            this.listener = context
        } else {
            throw RuntimeException("$context must implement MainFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initVisualizersPanel()
        initColorsDialog()
        initStylingPanel()

        val moreCoolAppsBtn = view.findViewById<TextView>(R.id.mainFragCoolAppsBtn)
        moreCoolAppsBtn.setOnClickListener {
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://dev?id=8456795065374888880")
                    )
                )
            } catch (e: ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/dev?id=8456795065374888880")
                    )
                )
            }
        }


        requestMicPermission()
//        playMusic("https://live-edge.rtl.lu/radio/eldo/playlist.m3u8")
    }

    private fun initStylingPanel() {
        stylingTitleBtn = view?.findViewById<TextView>(R.id.mainFragSettingsBtn)
        stylingPanel = view?.findViewById<LinearLayout>(R.id.mainFragStylingPanel)

        densitySeekbar = view?.findViewById<SeekBar>(R.id.mainFragStylingDensitySeekbar)
        densityLayout = view?.findViewById<LinearLayout>(R.id.mainFragStylingDensityLayout)

        strokeSeekbar = view?.findViewById<SeekBar>(R.id.mainFragStylingStrokeSeekbar)
        strokeLayout = view?.findViewById<LinearLayout>(R.id.mainFragStylingStrokeSeekbarLayout)

        lineTypeRadioGroup = view?.findViewById<RadioGroup>(R.id.mainFragStylingLineTypeGroup)
        lineTypeRadioGroupFillBtn = view?.findViewById<RadioButton>(R.id.mainFragStylingFillBtn)
        lineTypeRadioGroupOutlineBtn =
            view?.findViewById<RadioButton>(R.id.mainFragStylingOutlineBtn)

        speedRadioGroup = view?.findViewById<RadioGroup>(R.id.mainFragStylingSpeedGroup)
        speedRadioGroupSlowBtn = view?.findViewById<RadioButton>(R.id.speedRadioGroupSlowBtn)
        speedRadioGroupMediumBtn = view?.findViewById<RadioButton>(R.id.speedRadioGroupMediumBtn)
        speedRadioGroupFastBtn = view?.findViewById<RadioButton>(R.id.speedRadioGroupFastBtn)

        glowCheckBox = view?.findViewById<CheckBox>(R.id.mainFragStylingGlowCheckbox)

        glowCheckBox?.setOnCheckedChangeListener { buttonView, isChecked ->
            Log.d("wow", "initStylingPanel: listener is null ? = ${listener == null}")
            listener?.onSetColor(getLastUpdatedColor())
            listener?.showGlowBackground(isChecked)
            buttonView.isActivated = isChecked
        }

        stylingTitleBtn?.setOnClickListener {
            toggleStylingPanel()
        }
        densitySeekbar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (currentVisualizer !is HiFiVisualizer) {
                    currentVisualizer?.setDensity(
                        min(
                            0.99f, max(
                                0.01f,
                                progress.toFloat() % 100 / 100
                            )
                        )
                    )
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
        strokeSeekbar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
//                if (currentVisualizer !is HiFiVisualizer) {
                currentVisualizer?.setStrokeWidth(
                    min(
                        0.99f, max(
                            0.01f,
                            progress.toFloat() % 100 / 100
                        )
                    )
                )
//                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        lineTypeRadioGroup?.setOnCheckedChangeListener { group, checkedId ->
            lineTypeRadioGroupFillBtn?.isActivated = false
            lineTypeRadioGroupOutlineBtn?.isActivated = false

            val paintStyle = if (lineTypeRadioGroupFillBtn?.isChecked == true) {
                lineTypeRadioGroupFillBtn?.isActivated = true
                PaintStyle.FILL
            } else {
                lineTypeRadioGroupOutlineBtn?.isActivated = true
                PaintStyle.OUTLINE
            }

            currentVisualizer?.setPaintStyle(paintStyle)
        }
        speedRadioGroup?.setOnCheckedChangeListener { group, checkedId ->
            speedRadioGroupSlowBtn?.isActivated = false
            speedRadioGroupMediumBtn?.isActivated = false
            speedRadioGroupFastBtn?.isActivated = false

            val animSpeed = when {
                speedRadioGroupSlowBtn?.isChecked == true -> {
                    speedRadioGroupSlowBtn?.isActivated = true
                    AnimSpeed.SLOW
                }
                speedRadioGroupMediumBtn?.isChecked == true -> {
                    speedRadioGroupMediumBtn?.isActivated = true
                    AnimSpeed.MEDIUM
                }
                speedRadioGroupFastBtn?.isChecked == true -> {
                    speedRadioGroupFastBtn?.isActivated = true
                    AnimSpeed.FAST
                }
                else -> {
                    speedRadioGroupMediumBtn?.isActivated = true
                    AnimSpeed.MEDIUM
                }
            }
            currentVisualizer?.setAnimationSpeed(animSpeed)
        }
    }

    fun toggleStylingPanel() {
        stylingTitleBtn?.isActivated = !stylingTitleBtn?.isActivated!!
        colorsTitleBtn?.isActivated = false
        visualizersTitleBtn?.isActivated = false
        visualizersPanelRv?.visibility = View.GONE

        stylingPanel?.visibility = if (stylingTitleBtn?.isActivated == true) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    @SuppressLint("ClickableViewAccessibility")
//    private fun setSwipes() {
//        view?.setOnTouchListener(swipeTouchListener)
////        visualizationsPanel?.setOnTouchListener(swipeTouchListener)
////        for(visualizer in visualizers) {
////            visualizer.setOnTouchListener(swipeTouchListener)
////        }
//    }

    private val swipeTouchListener = object : OnSwipeTouchListener(context) {
        override fun onSwipeRight() {
            Log.d("onSwipe", "onSwipeRight")

//                    playVisualizer(currentVisualizer, true)

            val rnd = Random
            val randColor = Color.argb(
                255,
                rnd.nextInt(242) + 10,
                rnd.nextInt(242) + 10,
                rnd.nextInt(242) + 10
            )
            oldLastColor = lastColor
            lastColor = randColor

            changeColor(randColor, true)


        } // not in use

        override fun onSwipeLeft() {
            Log.d("onSwipe", "onSwipeLeft")

//                    playVisualizer(currentVisualizer, false)
            val tmpColor = lastColor
            changeColor(oldLastColor, false)

            lastColor = oldLastColor
            oldLastColor = tmpColor

        } // not in use

        override fun onSwipeTop() {
            Log.d("onSwipe", "onSwipeTop")
            toggleVisualizersPanel()
        }

        override fun onSwipeBottom() {
            Log.d("onSwipe", "onSwipeBottom")
            toggleVisualizersPanel()
        }
    }

    private fun changeColor(color: Int, shouldRandomizeGlowBg: Boolean = true) {
        currentVisualizer?.setColor(color)
        listener?.onSetColor(color)

        val showGlowBg = shouldRandomizeGlowBg && Random.nextBoolean()
        if (shouldRandomizeGlowBg) {
            glowCheckBox?.isActivated = showGlowBg
            glowCheckBox?.isChecked = showGlowBg
            listener?.showGlowBackground(showGlowBg)
        } else {
            listener?.showGlowBackground(glowCheckBox?.isActivated == true || glowCheckBox?.isChecked == true)
        }

        //colorize APP
        speedRadioGroupSlowBtn?.buttonTintList = ColorStateList.valueOf(color)
        speedRadioGroupMediumBtn?.buttonTintList = ColorStateList.valueOf(color)
        speedRadioGroupFastBtn?.buttonTintList = ColorStateList.valueOf(color)


        lineTypeRadioGroupOutlineBtn?.buttonTintList = ColorStateList.valueOf(color)
        lineTypeRadioGroupFillBtn?.buttonTintList = ColorStateList.valueOf(color)
        speedRadioGroupSlowBtn?.highlightColor = color
        speedRadioGroupMediumBtn?.highlightColor = color
        speedRadioGroupFastBtn?.highlightColor = color

        lineTypeRadioGroupOutlineBtn?.highlightColor = color
        lineTypeRadioGroupFillBtn?.highlightColor = color


        strokeSeekbar?.progressDrawable?.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        densitySeekbar?.progressDrawable?.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)

        strokeSeekbar?.thumb?.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        densitySeekbar?.thumb?.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)

        glowCheckBox?.buttonDrawable?.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)

//        speedRadioGroupSlowBtn
//        speedRadioGroupMediumBtn
//        speedRadioGroupFastBtn
    }

    private fun initColorsDialog() {
        colorsTitleBtn = view?.findViewById<TextView>(R.id.mainFragColorsBtn)

        colorsTitleBtn?.onFocusChangeListener =
            View.OnFocusChangeListener { v, hasFocus ->
                colorsTitleBtn?.isActivated = false
            }

        colorsTitleBtn?.setOnClickListener {
            colorPickerDialog = ColorPickerPopup.Builder(context)
                .initialColor(getLastUpdatedColor())
                .enableBrightness(true) // Enable brightness slider or not
                .enableAlpha(true) // Enable alpha slider or not
                .okTitle("Select")
                .cancelTitle("Cancel")
                .showIndicator(true)
                .showValue(false)
                .build()

            colorsTitleBtn?.isActivated = true
            showColorPickerDialog()
        }
    }

    private fun showColorPickerDialog() {
        colorPickerDialog?.show(view, object : ColorPickerPopup.ColorPickerObserver() {
            override fun onColorPicked(color: Int) {
                colorsTitleBtn?.isActivated = false
                colorPickerDialog = null

                oldLastColor = lastColor
                lastColor = color
                changeColor(color, false)
            }
        })
    }

    private fun getLastUpdatedColor(): Int {
        return if (lastColor != -1) {
            lastColor
        } else {
            when {
                oldLastColor != -1 -> {
                    oldLastColor
                }
                context != null -> {
                    ContextCompat.getColor(context!!, R.color.colorPrimary)
                }
                else -> {
                    Color.WHITE
                }
            }
        }
    }

    private fun initVisualizersPanel() {
        visualizationsPanel = view?.findViewById<LinearLayout>(R.id.mainFragVisualizersPanel)
        visualizersPanelRv = view?.findViewById<RecyclerView>(R.id.mainFragVisualizersPanelRv)
        visualizersTitleBtn = view?.findViewById<TextView>(R.id.mainFragVisualizersBtn)

        visualizersPanelRv?.apply {
            visualizersAdapter = VisualizersAdapter(this@MainFragment)
            adapter = visualizersAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        }

//        colorsTitleBtn
        visualizersTitleBtn?.setOnClickListener {
            toggleVisualizersPanel()
        }
    }

    fun toggleVisualizersPanel() {
        visualizersTitleBtn?.let { visualizersBtn ->
            visualizersBtn.isActivated = !visualizersBtn.isActivated

            stylingPanel?.visibility = View.GONE
            stylingTitleBtn?.isActivated = false
            colorsTitleBtn?.isActivated = false

            if (visualizersBtn.isActivated) {
                visualizersPanelRv?.visibility = View.VISIBLE
            } else {
                visualizersPanelRv?.visibility = View.GONE
            }
        }
    }

    private fun initObservers() {
        viewModel.currentRadioUrl.observe(
            viewLifecycleOwner,
            { radioUrl -> handleRadioUrl(radioUrl) })
    }

    private fun handleRadioUrl(radioUrl: String?) {
        if (!radioUrl.isNullOrEmpty()) {
            playMusic(radioUrl)
        }
    }

    private fun requestMicPermission() {
        if (activity == null) return
        val micPermissionGranted = isMicPermissionGranted()
        setVisualizersBtnVisibility(micPermissionGranted)

        if (micPermissionGranted) {
            initAllVisualizers()
        } else {
            askForMicPermission()
        }
    }

    fun initAllVisualizers() {
        mVisualizer = view?.findViewById<BlastVisualizer>(R.id.musicVisualizer)
        mVisualizer2 = view?.findViewById<BlobVisualizer>(R.id.musicVisualizer2)
        mVisualizer3 = view?.findViewById<BarVisualizer>(R.id.musicVisualizer3)
        mVisualizer4 = view?.findViewById<CircleLineVisualizer>(R.id.musicVisualizer4)
        mVisualizer5 = view?.findViewById<HiFiVisualizer>(R.id.musicVisualizer5)
        mVisualizer6 = view?.findViewById<WaveVisualizer>(R.id.musicVisualizer6)


        visualizers.clear()
        visualizers.add(mVisualizer!!)
        visualizers.add(mVisualizer2!!)
        visualizers.add(mVisualizer3!!)
        visualizers.add(mVisualizer4!!)
        visualizers.add(mVisualizer5!!)
        visualizers.add(mVisualizer6!!)

//        setSwipes()
        initAllClickListeners()
    }

    private fun initAllClickListeners() {
        for (visualizer in visualizers) {
            visualizer.setOnClickListener {
//                currentVisualizer?.visibility = View.GONE
//                visualizer.visibility = View.INVISIBLE
                when {
                    visualizersTitleBtn?.isActivated == true -> {
                        toggleVisualizersPanel()
                    }
                    stylingTitleBtn?.isActivated == true -> {
                        toggleStylingPanel()
                    }
                    colorsTitleBtn?.isActivated == true -> {
                        colorsTitleBtn?.isActivated = false
//                        toggleStylingPanel()
                    }
                    else -> {
                        playNextVisualizer()
                    }
                }
            }
            visualizer.setOnTouchListener(swipeTouchListener)
        }
    }

    private val focusChangeListener =
        OnAudioFocusChangeListener { focusChange ->
            val am = context?.getSystemService(Context.AUDIO_SERVICE) as AudioManager?
            when (focusChange) {
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK ->                            // Lower the volume while ducking.
                    mediaPlayer!!.setVolume(0.2f, 0.2f)
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> mediaPlayer?.pause()
                AudioManager.AUDIOFOCUS_LOSS -> {
                    mediaPlayer?.stop()
                    val component = ComponentName(
                        activity!!,
                        MainActivity::class.java
                    )
                    am!!.unregisterMediaButtonEventReceiver(component)
                }
                AudioManager.AUDIOFOCUS_GAIN -> {
                    // Return the volume to normal and resume if paused.
                    mediaPlayer!!.setVolume(1f, 1f)
                    mediaPlayer!!.start()
                }
                else -> {
                }
            }
        }

    override fun onStart() {
        super.onStart()
        activity?.let { viewModel.fetchRemoteConfigData(it) }
    }

    private fun playMusic(url: String) {
        if(mediaPlayer == null) {
            mediaPlayer = MediaPlayer()
        }
        try {
            val am = context?.getSystemService(Context.AUDIO_SERVICE) as AudioManager?
// Request audio focus for playback
            val result = am!!.requestAudioFocus(
                focusChangeListener,  // Use the music stream.
                AudioManager.STREAM_MUSIC,  // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN
            )

            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                Log.d("AudioFocus", "Audio focus received")

                mediaPlayer?.apply {
                    setAudioAttributes(
                        AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_MEDIA)
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .setLegacyStreamType(AudioManager.STREAM_MUSIC)
//                    .se
                            .build()
                    )
                    setOnPreparedListener { mp ->
                        //ON PLAY START
                        start()
                        //only if we have MIC permission do this ::
                        if (isMicPermissionGranted()) {     // set initial state
                            currentVisualizer = mVisualizer
                            initMusicVFX(mVisualizer)
                            visualizersAdapter.currentlySelectedIndex = 0
                            visualizersAdapter.notifyDataSetChanged()
                            lineTypeRadioGroupFillBtn?.isChecked = true
                            speedRadioGroupMediumBtn?.isChecked = true
                        }
//                    initMusicVFX(getRandomVisualizer())
                    }
                    setOnErrorListener { mp, what, extra ->
                        FirebaseCrashlytics.getInstance()
                            .recordException(Exception("CUSTOMIZED EXCEPTION :: mediaPlayer -> what is=$what, and extra is=$extra"))
                        false
                    }
                    setOnCompletionListener {
//                    mVisualizer?.hide()
                        for (visualizer in visualizers) {
                            (visualizer as BaseVisualizer).hide()
                        }
                    }
                    setDataSource(url)
                    prepareAsync()
                }
            } else {
                Log.d("AudioFocus", "Audio focus NOT received")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            FirebaseCrashlytics.getInstance().recordException(e)
        }
    }

    private fun isMicPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            activity!!, Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun askForMicPermission() {
        ActivityCompat.requestPermissions(
            activity!!,
            arrayOf(Manifest.permission.RECORD_AUDIO),
            MainActivity.MICROPHONE_REQUEST_CODE
        )
    }

    private fun getRandomVisualizer(): BaseVisualizer? {
//        val nextInt = Random.nextInt(0, 1)
//        currentVisualizer = visualizers[nextInt]
//        currentVisualizer = when (nextInt) {
//            0 -> {
//                Toast.makeText(context, "First", Toast.LENGTH_SHORT).show()
//                mVisualizer
//            }
//            else -> {
//                Toast.makeText(context, "second", Toast.LENGTH_SHORT).show()
//                mVisualizer2
//            }
//        }

        currentVisualizer?.release()
        currentVisualizer = visualizers.random() as BaseVisualizer
//        Toast.makeText(context, nextInt.toString(), Toast.LENGTH_SHORT).show()
        return currentVisualizer
    }
//
//    private fun playMusic(url: String) {
//        context?.let{
//            val uri = Uri.parse(url)
//            val audioAttributes = AudioAttributes.Builder()
//                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
//                .build()
////            mediaPlayer = MediaPlayer.create(it, uri)
//
//            mediaPlayer?.apply {
//                setAudioAttributes(audioAttributes)
////                setAudioStreamType(AudioManager.STREAM_MUSIC)
//                setDataSource(it, uri)
////                setDataSource(url)
//                prepare()
//
//                setOnPreparedListener {
//                    start()
//                }
//            }
//        }
//    }
//    private fun initMusicVFX() {
//        mVisualizer?.setOnClickListener {
////            randomizeColor()
//            mVisualizer?.apply {
//                val rnd = Random
//                val color: Int = Color.argb(
//                    255,
//                    rnd.nextInt(256),
//                    rnd.nextInt(256),
//                    rnd.nextInt(256)
//                )
//                setColor(color)
//                setPaintStyle(if (rnd.nextBoolean()) PaintStyle.OUTLINE else PaintStyle.FILL)
//            }
//        }
////        mVisualizer?.setOnLongClickListener {
////            randomizeColor()
////            false
////        }
//        mVisualizer?.setOnTouchListener { v, event ->
//            randomizeColor(currentVisualizer)
//            false
//        }
//        //get the AudioSessionId from your MediaPlayer and pass it to the visualizer
//        val audioSessionId: Int = mediaPlayer?.audioSessionId ?: -1
//        if (audioSessionId > 0)
//            mVisualizer?.setAudioSessionId(audioSessionId)
//    }

    private fun initMusicVFX(visualizer: BaseVisualizer?) {
        if(visualizer == null) return
        //get the AudioSessionId from your MediaPlayer and pass it to the visualizer
        val audioSessionId: Int = mediaPlayer?.audioSessionId ?: -1
        if (audioSessionId > 0) {
            try {
                visualizer.setAudioSessionId(audioSessionId)
            } catch (e: java.lang.Exception) {
                playNextVisualizer()
                FirebaseCrashlytics.getInstance().recordException(Exception("CUSTOMIZED EXCEPTION :: initMusicVFX -> $e"))

            }
        }
    }

    private fun clearAllVisualizers() {
        colorsTitleBtn?.isActivated = false
        for(visualizer in visualizers) {
            visualizer.visibility = View.INVISIBLE
        }
    }

    fun setVisualizersBtnVisibility(isVisible: Boolean) {
        visualizersTitleBtn?.visibility = if(isVisible) {
            View.VISIBLE
        } else {
            View.GONE
        }
        stylingTitleBtn?.visibility = visualizersTitleBtn?.visibility ?: View.GONE
        colorsTitleBtn?.visibility = visualizersTitleBtn?.visibility ?: View.GONE
    }

    private fun playVisualizer(visualizer: BaseVisualizer?, isRandomizedColor: Boolean) {
        visualizer?.apply {
            visibility = View.VISIBLE
            initMusicVFX(visualizer)

            val rnd = Random
            oldLastColor = lastColor
            lastColor = if(isRandomizedColor) {
                Color.argb(255, rnd.nextInt(242) + 10, rnd.nextInt(242) + 10, rnd.nextInt(242) + 10)
            } else {
                getLastUpdatedColor()
            }

            changeColor(lastColor, isRandomizedColor)
//            setColor(lastColor)
//            listener?.onSetColor(lastColor)

            val animSpeed = if(rnd.nextBoolean() && rnd.nextBoolean()) {
                speedRadioGroupFastBtn?.isChecked = true
                AnimSpeed.FAST
            } else if (rnd.nextBoolean()) {
                speedRadioGroupSlowBtn?.isChecked = true
                AnimSpeed.SLOW
            } else {
                speedRadioGroupMediumBtn?.isChecked = true
                AnimSpeed.MEDIUM
            }
            setAnimationSpeed(animSpeed)

            if(visualizer !is HiFiVisualizer) {
                val density: Float = max(0.2f, rnd.nextFloat())
                densityLayout?.visibility = View.VISIBLE
//                strokeLayout?.visibility = View.VISIBLE
                densitySeekbar?.progress = (density * 100).roundToInt()
                setDensity(density)
            } else {
                densityLayout?.visibility = View.GONE
//                strokeLayout?.visibility = View.GONE
            }
            val paintStyle = if (rnd.nextBoolean()) PaintStyle.OUTLINE else PaintStyle.FILL
            setPaintStyle(paintStyle)

            if (paintStyle == PaintStyle.FILL) {
                lineTypeRadioGroupFillBtn?.isChecked = true
                lineTypeRadioGroupOutlineBtn?.isChecked = false
            } else {
                lineTypeRadioGroupFillBtn?.isChecked = false
                lineTypeRadioGroupOutlineBtn?.isChecked = true
            }

            visualizersAdapter.currentlySelectedIndex = when (visualizer) { //visualizer
                is BlastVisualizer -> {
                    0
                }
                is BlobVisualizer -> {
                    1
                }
                is BarVisualizer -> {
                    2
                }
                is CircleLineVisualizer -> {
                    3
                }
                is HiFiVisualizer -> {
                    4
                }
                is WaveVisualizer -> {
                    5
                }
                else -> {
                    -1
                }
            }
            visualizersAdapter.notifyDataSetChanged()

            //todo add update to visualizers panel
        }
    }

    fun playNextVisualizer() {
        Log.d("wow", "playNextVisualizer")
        clearAllVisualizers()
        playVisualizer(getRandomVisualizer(), true)
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer?.release()

        releaseAllVisualizers()
    }

    private fun releaseAllVisualizers() {
        for(visualizer in visualizers) {
            (visualizer as BaseVisualizer).release()
        }
    }

    override fun onVisualizerSelected(visualizer: Visualizer) {
        if(visualizer.index == -1) {        //Normal

//            currentVisualizer?.release()
            clearAllVisualizers()
            releaseAllVisualizers()

//            currentVisualizer = mVisualizer
            mVisualizer?.visibility = View.VISIBLE
//            mVisualizer2?.visibility = View.VISIBLE

            context?.let {
                val currentColor = ContextCompat.getColor(it, R.color.colorPrimary)
                mVisualizer?.setColor(currentColor)
                listener?.onSetColor(currentColor)
                listener?.showGlowBackground(false)

//                mVisualizer2?.setColor(ContextCompat.getColor(it, R.color.colorPrimaryDark))

                mVisualizer?.setPaintStyle(PaintStyle.OUTLINE)
//                mVisualizer2?.setPaintStyle(PaintStyle.OUTLINE)
//                setPaintStyle(if (rnd.nextBoolean()) PaintStyle.OUTLINE else PaintStyle.FILL)


                mVisualizer?.setDensity(0.8f)
//                mVisualizer2?.setDensity(0.8f)
            }

        } else {
            val visualizerView = visualizers[visualizer.index]
            val baseVisualizer = visualizerView as BaseVisualizer




            val baseVisualizerIndex = when (baseVisualizer) { //visualizer
                is BlastVisualizer -> {
                    isFirstTimeClickingOnItemTmp = currentVisualizer !is BlastVisualizer
                    0
                }
                is BlobVisualizer -> {
                    isFirstTimeClickingOnItemTmp = currentVisualizer !is BlobVisualizer
                    1
                }
                is BarVisualizer -> {
                    isFirstTimeClickingOnItemTmp = currentVisualizer !is BarVisualizer

                    2
                }
                is CircleLineVisualizer -> {
                    isFirstTimeClickingOnItemTmp = currentVisualizer !is CircleLineVisualizer

                    3
                }
                is HiFiVisualizer -> {
                    isFirstTimeClickingOnItemTmp = currentVisualizer !is HiFiVisualizer

                    4
                }
                is WaveVisualizer -> {
                    isFirstTimeClickingOnItemTmp = currentVisualizer !is WaveVisualizer
                    5
                }
                else -> {
                    isFirstTimeClickingOnItemTmp = false
                    -1
                }
            }
            val shouldRandomizeColorsOnClick = if(baseVisualizerIndex != visualizer.index) {
                isFirstTimeClickingOnItemTmp = true
                true
            } else {
//                isFirstTimeClickingOnItemTmp = false
                false
            }

            currentVisualizer?.release()
            currentVisualizer = baseVisualizer

            clearAllVisualizers()
            playVisualizer(
                baseVisualizer,
                shouldRandomizeColorsOnClick || !isFirstTimeClickingOnItemTmp
            )
        }
    }

    private var isFirstTimeClickingOnItemTmp = false

    fun isVisualizersPanelOpen(): Boolean {
        return visualizersPanelRv?.visibility == View.VISIBLE && visualizersTitleBtn?.isActivated == true
    }

    fun isStylingPanelOpen(): Boolean {
        return stylingPanel?.visibility == View.VISIBLE && stylingTitleBtn?.isActivated == true
    }


    fun closeColorPickerDialogIfOpen(): Boolean {
        return if (colorPickerDialog != null) {
            colorPickerDialog!!.dismiss()
            colorPickerDialog = null
            colorsTitleBtn?.isActivated = false
            true
        } else {
            false
        }
    }
}